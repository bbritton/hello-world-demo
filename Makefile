CPP = g++

FLAGS = -Wall -Werror

helloWorld: helloWorld.o
	$(CPP) $(FLAGS) -o helloWorld helloWorld.o

helloWorld.o: helloWorld.cpp
	$(CPP) $(FLAGS) -c helloWorld.cpp

clean:
	rm -fr *.o helloWorld